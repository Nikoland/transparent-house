var house   = document.querySelectorAll('.house')[0],
    houseBg = document.querySelectorAll('.houseBG')[0],
    procX   = window.innerWidth * 0.5,
    procY   = window.innerHeight * 0.5;

window.onresize = function (event) {
    procX = window.innerWidth * 0.5;
    procY = window.innerHeight * 0.5;
    
    setTimeout(function () {
        initEvents()
    }, 10);
};

var hmX, hmY, hmZ;
function houseMove(e) {
    hmZ = 0;
    hmX = ((e.x - procX) * 0.04 * 2).toFixed(2);
    hmY = ((e.y - procY) * 0.03 * 2).toFixed(2);
    transformHouse(-hmY, hmX, -hmZ)
}

var hmdX, hmdY, hmdZ;
function handleMotionEvent(event) {
    hmdX = event.beta - 55;
    hmdY = event.gamma * 0.4;
    hmdZ = 0;
    
    if (hmdY >= 30) hmdY = 30;
    if (hmdY <= -30) hmdY = -30;
    
    if (hmdX >= 30) hmdX = 30;
    if (hmdX <= -45) hmdX = -45;
    
    transformHouse(hmdX, hmdY + 30, hmdZ)
}

var shadow = {
    front: document.querySelector('.wall--2 .wall__shadow'),
    back: document.querySelector('.wall--1 .wall__shadow'),
    offset: 5,
    up: 1.3
};
shadow.back.style.opacity = 0;

shadow.front.style.opacity = 1;
shadow.front.style.with = '50%';
shadow.front.style.left = '10%';
shadow.front.style.transform = shadow.front.style.webkitTransform = 'translateX(-50%)  rotateX(90deg) rotateZ(26deg)';

function transformHouse(x, y, z) {
    shadow.back.style.opacity = shadow.front.style.opacity = 0;
    if (x < -shadow.offset){
        shadow.front.style.opacity = 1;
    }
    if (x > shadow.offset){
        shadow.back.style.opacity = 1;
    }
    shadow.front.style.width = shadow.back.style.width = 100 - Math.abs(y/2 * shadow.up) + '%';
    shadow.front.style.top = shadow.back.style.top = 105 - Math.abs(x/2) + '%';
    shadow.front.style.left = 50 - (y/2 * shadow.up) + '%';
    shadow.back.style.left = 50 + (y/2 * shadow.up) + '%';
    shadow.front.style.transform = shadow.back.style.transform = shadow.front.style.webkitTransform = shadow.back.style.webkitTransform = 'translateX(-50%)  rotateX(90deg) rotateZ(' + y + 'deg)';
    
    house.style.transform = house.style.webkitTransform = 'rotateX(' + x + 'deg) rotateY(' + y + 'deg) rotateZ(' + z + 'deg)';
}

function transitionHouse(str) {
    house.style.transition = house.style.webkitTransition = str;
}

function initEvents() {
    if (window.innerWidth <= 960) {
        window.removeEventListener('mousemove', houseMove, false);
        transitionHouse('0.7s all');
        if (window.DeviceOrientationEvent) {
            window.addEventListener('deviceorientation', handleMotionEvent, false)
        }
    } else {
        transitionHouse('none');
        window.addEventListener('mousemove', houseMove, false);
        window.removeEventListener('deviceorientation', handleMotionEvent, false);
    }
}

setTimeout(function() {
    initEvents();
}, 4250);
