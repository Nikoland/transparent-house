(function() {
    Array.prototype.shuffle = function() {
        for (var i = this.length - 1; i > 0; i--) {
            var num = Math.floor(Math.random() * (i + 1));
            var d = this[num];
            this[num] = this[i];
            this[i] = d;
        }
        return this;
    };

    var path = document.querySelectorAll('.houseBG path');
    [].forEach.call(path, function (item) {
        var length = item.getTotalLength();
        item.style.transition = item.style.WebkitTransition = 'none';
        item.style.strokeDasharray = item.style.WebkitStrokeDasharray = length + ' ' + length;
        item.style.strokeDashoffset = item.style.WebkitStrokeDashoffset = length;
        item.getBoundingClientRect();
        item.style.transition = item.style.WebkitTransition = 'stroke-dashoffset ' + item.dataset.speed + ' linear ' + item.dataset.delay;
        /* item.style.transition = item.style.WebkitTransition = 'stroke-dashoffset ' + item.getAttribute('data-speed') + ' linear ' + item.getAttribute('data-delay'); */
    });

    var navLevel = document.querySelectorAll('.navLevel'), delayArray = ['0.25s','0.5s','0.75s','1s'];
    [].forEach.call(navLevel, function(el) {
        var i = 0;
        delayArray.shuffle();

        [].forEach.call(el.querySelectorAll('.navLevel__item'), function(ell) {
            ell.style.animationDelay = delayArray[i];
            ell.style.webkitAnimationDelay = delayArray[i];
            i++;
        });
    });

    var socials = document.querySelectorAll('.header__soc .iconic'), delayArrayLong = ['4.4s','4.6s','4.8s','5.0s', '5.2s', '5.4s'], i = 0;

    [].forEach.call(socials, function (el) {
        el.style.animationDelay = delayArrayLong[i];
        el.style.webkitAnimationDelay = delayArrayLong[i];

        i++;
    });

    var navItem = document.querySelectorAll('.nav__item span'),
        page = document.querySelector('.page'),
        video = document.querySelectorAll('.videoBg'),
        closeNavLevelItem = document.querySelectorAll('.videoWrap, .header, .nav i');

    [].forEach.call(navItem, function (item) {
        item.addEventListener('click', navActive)
    });

    [].forEach.call(closeNavLevelItem, function (item) {
        item.addEventListener('click', navReset)
    });
    
    function navActive(){
        var th, dataVideo;
        
        navReset();
        page.classList.add('active');
        [].forEach.call(navItem, function (item) {
            item.parentNode.classList.add('hide')
        });
        th = this.parentNode;
        th.classList.remove('hide');
        th.classList.add('active');
        dataVideo = th.getAttribute('data-video');
        document.getElementById(dataVideo).classList.add('active');
        document.getElementById(dataVideo).play();
    }

    function navReset() {
        [].forEach.call(navItem, function (item) {
            item.parentNode.classList.remove('active');
            item.parentNode.classList.remove('hide')
        });
        
        [].forEach.call(video, function (item) {
            item.classList.remove('active')
        });
        
        page.classList.remove('active')
    }    

    window.onload = function() {
        page.classList.add('load'); 

        [].forEach.call(path, function (item) {
            item.style.strokeDashoffset = '0';
        });       
    }

}).call(this);
